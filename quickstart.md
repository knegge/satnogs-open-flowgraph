# Quick start guide

This is a quick guide on building and running this flowgraph under docker.<br>
For now it's a few steps but will perhaps be more integrated in the future.<br>
It is based on different repos on gitlab, please see respective repo what is changed.<br>
Please note, make sure your host has [docker compose](https://docs.docker.com/compose/) installed, 
it is either called with `docker-compose` or `docker compose`, this guide will specify without the hyphen,
adjust for your needs. Docker compose v1.27 minimum, but the v2 is recommended.<br>

This repo is likely to change a lot as it is very new, it is not complete in any way, and it will take time to get
everything in a working condition.

## Building
First build the gnuradio image, this is based on LSF gnuradio-3.8 on bullseye, updated to gnuradio-3.10 on bookworm.
```shell
git clone -b maint-3.10 https://gitlab.com/knegge/docker-gnuradio.git
cd docker-gnuradio
docker compose build gnuradio_satnogs
docker images|grep ^gnuradio
```

Next fetch and build this repo.
```shell
git clone https://gitlab.com/knegge/satnogs-open-flowgraph.git
cd satnogs-open-flowgraph
cp station.env-dist station.env
docker compose build
docker images|grep ^satnogs-client
```

## Configuring
Edit the `station.env` and make sure to populate with your settings.<br>
Important, remove "driver=" from the device string, which was used in the Soapy source from previous versions.<br> 
For example: `SATNOGS_SOAPY_RX_DEVICE=rtlsdr`

I recommend using this only on [network-dev](https://network-dev.satnogs.org/), 
please create a station there if you don't already have one.<br>
This is selected in the config with the following variables:
```shell
SATNOGS_NETWORK_API_URL=https://network-dev.satnogs.org/api/
SATNOGS_ARTIFACTS_API_URL=https://db-dev.satnogs.org/api/
```

## Running
To start the client in foreground: `docker compose up` and stopping it with Ctrl-C.<br>
When you know it is running properly, you can simply add `-d` to the up command, this makes it detach from the console.
Stopping and removing the container with: `docker compose down`

## Mode of operation
The official satnogs-client is used and not modified. Althou rigctld is not used it only results in a warning.<br>
There's a [script](satnogs_wrapper.py) that runs before and after
every observation, and it handles the generation of doppler compensation and post-processing of observation results.<br>
The new flowgraph has all the parameters as the old versions to be compatible, some of these are deprecated.<br>
The idea of the flowgraph is to use commonly available blocks plus gr-satellites for
[doppler compensation](https://destevez.net/2022/07/real-time-doppler-correction-with-gnu-radio/).<br>
UDP is used to make the IQ samples available for any demodulator, for now the script is launching
[gr_satellites cli](https://gr-satellites.readthedocs.io/en/latest/command_line.html), and it has a list of
[supported satellites](https://gr-satellites.readthedocs.io/en/latest/supported_satellites.html).<br>
If you want to add more supported satellites, please check out the
[SatYAML](https://gr-satellites.readthedocs.io/en/latest/satyaml.html) on how to do this.
