#!/bin/sh
DOCKER_XAUTH=/tmp/.docker.xauth
IMAGE=satnogs-client:open-flowgraph
touch ${DOCKER_XAUTH}
xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f $DOCKER_XAUTH nmerge -
docker run -it --rm \
    --net=host \
    -e DISPLAY=$DISPLAY \
    -e XAUTHORITY=$DOCKER_XAUTH \
    -e DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$(id -u)/bus \
    -v /tmp/.docker.xauth:/tmp/.docker.xauth:ro \
    -v /tmp/.X11-unix:/tmp/.X11-unix:ro \
    -v /run/user/$(id -u)/bus:/run/user/$(id -u)/bus:ro \
    -v $(pwd)/build:/build \
    -v $(pwd)/station.env:/.env \
    --device=/dev/bus/usb/ \
    --tmpfs /tmp \
    ${IMAGE} \
    gnuradio-companion
