#!/usr/bin/env python3
import base64
import datetime
import ephem
import json
import os
import re
import shutil
import struct
import subprocess
import sys

# station configuration needs:
# SATNOGS_PRE_OBSERVATION_SCRIPT="satnogs-pre {{ID}} {{FREQ}} {{TLE}} {{TIMESTAMP}} {{BAUD}} {{SCRIPT_NAME}}"
# SATNOGS_POST_OBSERVATION_SCRIPT="satnogs-post {{ID}} {{FREQ}} {{TLE}} {{TIMESTAMP}} {{BAUD}} {{SCRIPT_NAME}}"
# Make symlinks for satnogs-pre and satnogs-post to point to this script

# Set manualmode to 1 to simulate -pre, 2 to simulate -post
try:
    manualmode = int(os.getenv('MANUALMODE', 0))
    verbose = int(os.getenv('VERBOSITY', 0))
    keepfiles = int(os.getenv('KEEPFILES', 0))
except ValueError:
    print('Warning: unable to parse MANUALMODE, VERBOSITY, KEEPFILES. Using defaults.')
    manualmode = 0
    verbose = 0
    keepfiles = 0

# Hard coded values for now, perhaps use satnogs-config in the future
if manualmode > 0:
    TMP = './build'
    DATA = './build'
else:
    TMP = '/tmp/.satnogs'        # SATNOGS_APP_PATH
    DATA = '/tmp/.satnogs/data'  # SATNOGS_OUTPUT_PATH


def main():
    env = {'host': os.getenv('UDP_DUMP_HOST', '0.0.0.0'),
           'port': os.getenv('UDP_DUMP_PORT', '57356'),
           'length': 3600,
           'rate': '48e3'}
    try:
        env['lat'] = os.getenv('SATNOGS_STATION_LAT')
        env['lon'] = os.getenv('SATNOGS_STATION_LON')
        env['elev'] = float(os.getenv('SATNOGS_STATION_ELEV'))
        env['rx_rate'] = os.getenv('SATNOGS_RX_SAMP_RATE')
        env['iq'] = os.getenv('IQ_DUMP_FILENAME', f'{TMP}/iq.wav')
        env['home'] = os.getenv('HOME', '/')
    except TypeError:
        print('Missing station configuration.')
        exit(0)
    if len(sys.argv) != 7:
        print('Wrong number of arguments, expected {{ID}} {{FREQ}} {{TLE}} {{TIMESTAMP}} {{BAUD}} {{SCRIPT_NAME}}')
        exit(0)

    try:
        env['id'] = sys.argv[1]
        env['freq'] = float(sys.argv[2])
        env['tle'] = json.loads(sys.argv[3])
        env['ts'] = sys.argv[4]
        env['baud'] = sys.argv[5]
        env['script'] = sys.argv[6]
    except TypeError:
        print('Bad data in arguments.')
        exit(0)
    env['wf'] = f'{DATA}/receiving_waterfall_{env["id"]}_{env["ts"]}.dat-raw'

    if verbose > 0:
        print(env)
    try:
        if keepfiles > 0:
            with open(f'{env["home"]}/wrapper.log', 'a') as lf:
                lf.write(f'{env}\n')
    except Exception as e:
        print(f'Unable to write wrapper.log {e}')

    if 'satnogs-pre' in sys.argv[0] or manualmode == 1:
        print('Satnogs wrapper pre processing')
        generate_doppler(env)
        start_grsat(env)
    elif 'satnogs-post' in sys.argv[0] or manualmode == 2:
        print('Satnogs wrapper post processing')
        remove_doppler(env)
        stop_grsat(env)
        convert_waterfall(env)
        manage_iqfile(env)
    else:
        print('Unknown script name, call as satnogs-pre or satnogs-post')


def generate_doppler(env):
    # Set site information
    observer = ephem.Observer()
    observer.lat = env['lat']
    observer.lon = env['lon']
    observer.elevation = env['elev']

    # Set satellite
    try:
        sat = ephem.readtle(env['tle']['tle0'], env['tle']['tle1'], env['tle']['tle2'])
    except ValueError:
        print('TLE read error.')
        # touch doppler.txt ?
        return

    # Set time
    tstart = datetime.datetime.strptime(env['ts'], '%Y-%m-%dT%H-%M-%S').replace(tzinfo=datetime.timezone.utc)
    times = [tstart + datetime.timedelta(seconds=s) for s in range(0, env['length'])]

    # Compute Doppler and write
    if verbose > 0:
        print(f'Generating doppler for observation {env["id"]}, first timestamp {tstart.timestamp()}')
    with open(f'{TMP}/doppler.txt', 'w') as fp:
        # ts = 0.0
        for t in times:
            observer.date = t
            sat.compute(observer)

            freq = (1 - sat.range_velocity / 299792458.0) * env['freq'] - env['freq']
            # print(f'{t} {freq} {sat.alt}')
            fp.write(f'{t.timestamp()} {freq}\n')
            # fp.write(f'{ts} {freq}\n')  # this will not work for timestamped usrp stream ?
            # ts += 1.0

            # Exit if satellite has set
            # if sat.alt < 0:
            #     break


def remove_doppler(env):
    if verbose > 0:
        print(f'Removing doppler for observation {env["id"]}')
    try:
        if keepfiles > 0:
            shutil.move(f'{TMP}/doppler.txt', f'{env["home"]}/{env["id"]}_doppler.txt')
        else:
            os.unlink(f'{TMP}/doppler.txt')
    except (FileNotFoundError, OSError):
        pass


def start_grsat(env):
    if manualmode > 0:  # testing under windows
        grbin = ['%UserProfile%\\radioconda\\python.exe', '%UserProfile%\\radioconda\\Library\\bin\\gr_satellites.py']
    else:
        grbin = ['gr_satellites']
    norad = env['tle']['tle2'].split(' ')[1]

    # TODO: reformat date Y-m-dTH-M-S to Y-m-dTH:M:S for use with --start_time

    # generate list of supported satellites. Takes time, should be cached, could be omitted ?
    satlist = subprocess.run(grbin + ['--list_satellites'], shell=True, capture_output=True, text=True)
    nlist = ' '.join([n[0] for n in [re.findall(r'\(NORAD (\d+)\)', n) for n in satlist.stdout.splitlines()] if n])
    if norad not in nlist:
        print(f'Satellite {norad} not supported')
    #    return

    grbin += [norad, '--samp_rate', env['rate'], '--iq', '--udp', '--udp_raw', '--udp_port', env['port'],
              '--kiss_out', f'{TMP}/grsat.kiss', '--ignore_unknown_args', '--use_agc', '--satcfg']

    # launch
    print(f'Starting gr_satellites for {norad}')
    print(' '.join(grbin))
    try:
        s = subprocess.Popen(grbin)
        with open(f'{TMP}/grsat.pid', 'w') as pf:
            pf.write(str(s.pid))
    except FileNotFoundError:
        print(f'Unable to launch {grbin[0]}')


def stop_grsat(env):
    print('Stopping gr_satellites')

    # kill
    try:
        with open(f'{TMP}/grsat.pid', 'r') as pf:
            os.kill(int(pf.readline()), 15)
        os.unlink(f'{TMP}/grsat.pid')
    except (FileNotFoundError, ProcessLookupError, OSError):
        print('No gr_satellites running')
        # return

    # Convert kiss to json data_
    try:
        with open(f'{TMP}/grsat.kiss', 'rb') as kf:
            print('Processing kiss file')
            dp = f"{DATA}/data_{env['id']}_"
            ext = 0
            for ts, frame in parse_kissfile(kf):
                if len(frame) == 0:
                    continue
                datafile = f'{dp}{ts.strftime("%Y-%m-%dT%H-%M-%S_g")}'
                while True:
                    if os.path.isfile(datafile + str(ext)):
                        ext += 1
                    else:
                        datafile += str(ext)
                        break
                data = {'decoder_name': 'gr-satellites',
                        'pdu': base64.b64encode(frame).decode()}
                with open(datafile, 'w') as df:
                    json.dump(data, df, default=str)
                if verbose > 1:
                    print(f'{datafile} len {len(frame)}')
        if keepfiles > 0:
            shutil.move(f'{TMP}/grsat.kiss', f'{env["home"]}/{env["id"]}_grsat.kiss')
        else:
            os.unlink(f'{TMP}/grsat.kiss')
    except (FileNotFoundError, OSError):
        print('No kiss data found')


def parse_kissfile(infile):
    ts = datetime.datetime.now()  # MUST be overwritten by timestamps in file
    for row in infile.read().split(b'\xC0'):
        if len(row) == 9 and row[0] == 9:  # timestamp frame
            ts = datetime.datetime(1970, 1, 1) + datetime.timedelta(seconds=struct.unpack('>Q', row[1:])[0] / 1000)
        if len(row) > 0 and row[0] == 0:  # data frame
            yield ts, row[1:].replace(b'\xdb\xdc', b'\xc0').replace(b'\xdb\xdd', b'\xdb')


def convert_waterfall(env):
    # hard coded values from flowgraph
    nchan = 1024
    samp_rate = 48000
    navg = 5

    try:
        fb = env['wf'].rsplit('.', 1)[0]
        ts = bytes(re.findall(r'(\d+-\d+-\d+T\d+-\d+-\d+)', fb)[0] + '.0Z', 'ascii')
        print(f'Converting raw to satnogs dat {fb}')
        with open(f'{fb}.dat', 'wb') as wf, open(env['wf'], 'rb') as dat:
            wf.write(struct.pack('32s', ts))  # timestamp
            wf.write(struct.pack('>i', nchan))  # nchan
            wf.write(struct.pack('>i', samp_rate))  # samp_rate
            wf.write(struct.pack('>i', navg))  # nfft_per_row
            wf.write(struct.pack('>f', env['freq']))  # center_freq
            wf.write(struct.pack('>i', 16777216))  # endianess
            n = 0
            while d := dat.read(nchan*4):
                wf.write(struct.pack('=q', n))
                wf.write(d)
                n += int(1e6 * nchan * navg / samp_rate)
    except Exception as e:
        print(f'Error {e}')
    try:
        if keepfiles > 0:
            shutil.move(env['wf'], f'{env["home"]}/{env["id"]}_wf.dat-raw')
        else:
            os.unlink(env['wf'])
    except (FileNotFoundError, OSError):
        pass


def manage_iqfile(env):
    try:
        if keepfiles > 1 and os.path.isfile(env['iq']):
            shutil.move(env['iq'], f'{env["home"]}/{env["id"]}_iq.wav')
        else:
            os.unlink(env['iq'])
    except (FileNotFoundError, OSError) as e:
        print(f'IQ move error {e}')


if __name__ == "__main__":
    main()
