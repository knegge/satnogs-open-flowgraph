#!/bin/bash
FGL="afsk1200_ax25 amsat_fox_duv_decoder argos_bpsk_ldr bpsk cw_decoder example_flowgraph fm fsk iq_receiver noaa_apt_decoder qubik_telem reaktor_hello_world_fsk9600_decoder sstv_pd120_demod"

grcc /usr/local/src/satnogs-client/satnogs_generic310.grc
chmod 0755 satnogs_generic.py
cp /usr/local/src/satnogs-client/satnogs_wrapper.py .
ln -s satnogs_wrapper.py satnogs-pre
ln -s satnogs_wrapper.py satnogs-post
for s in ${FGL}; do
    ln -s satnogs_generic.py "satnogs_${s}.py"
done
mkdir -p /etc/gnuradio/conf.d/
cp /usr/local/src/satnogs-client/udp.conf /etc/gnuradio/conf.d/

